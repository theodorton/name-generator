class NamesController < ApplicationController
  def new
    @name = Name.new
  end
  
  def create
    @name = Name.new(params[:name])
    render :new
  end
end