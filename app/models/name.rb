class Name
  attr_accessor :first_name, :last_name
  
  def initialize(attributes = {})
    attributes.each_pair do |k,v|
      send("#{k}=".to_sym, v)
    end
  end
end