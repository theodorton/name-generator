NameGenerator::Application.routes.draw do
  resources :names, only: [:new, :create]
  root to: redirect("/names/new")
end
